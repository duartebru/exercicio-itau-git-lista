package tech.mastertech.itau.exemplos;

public class Pessoa {
	private int idade;
	private String nome;
	
	// construtor - Overloading
	public Pessoa(String nome, int idade){
		this.nome = nome;
		this.idade = idade;
	}
	
	// mesmo construtor depende dos argumentos -  Overloading
	public Pessoa(int idade){
		this.nome = "Não identificado";
		this.idade = idade;
	}
	
	// mesmo construtor depende dos argumentos -  Overloading
	public Pessoa(){
		this.nome = "Não identificado";
	}	
	
	public String toString() {
		return "Meu nome é " + nome + " e tenho " + idade + " anos";
	}
	
}
