package tech.mastertech.itau.exemplos;

public class TiposDeDados {
	int numero = 9;
	double decimal = 0.14;
	boolean creditoOuDebito = true;
	char letra = 'a';
	
	// classe String
	String frase = "Meu nome é Bruna";
	
	// vetores
	int [] notas = {2, 5, 10, 20, 50, 100};
	int [] aindaNaoSei = new int[10];
		
}
	

